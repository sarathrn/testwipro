//
//  PaddingLabel.swift
//  TestWipro
//
//  Created by Sarath R on 26/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import UIKit

class PaddingLabel: UILabel {

    override func drawText(in rect: CGRect) {
        
        let inset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, inset))
    }
}
