//
//  LoadingView.swift
//  TestWipro
//
//  Created by Sarath R on 26/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation
import UIKit



class LoadingView: UIView {
    
    
    // Declarations
    fileprivate var gradient = CAGradientLayer()
    fileprivate var kDefaultColoredViewWidth: CGFloat = 100
    fileprivate lazy var coloredView: UIView = {
        
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: kDefaultColoredViewWidth, height: self.frame.height)
        return view
    }()
    
    
    // MARK: Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureView()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureView()
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateFrame()
        startAnimate()
    }
    
    
    fileprivate func configureView() {
        
        self.addSubview(coloredView)
        self.addShadow(color: .lightGray, offSet: CGSize(width: 1, height: 1))
    }
    
    
    fileprivate func updateFrame() {
        
        kDefaultColoredViewWidth = self.bounds.width / 2
        coloredView.frame.size = CGSize(width: kDefaultColoredViewWidth, height: self.bounds.height)
    }
    
    
    fileprivate func float() {
        
        UIView.animate(withDuration: 2, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            
            self.coloredView.frame.origin.x = self.bounds.size.width
            self.coloredView.layoutIfNeeded()
        }) { (status) in
            
            self.coloredView.frame.origin.x = -(self.kDefaultColoredViewWidth)
            self.coloredView.layoutIfNeeded()
            self.float()
        }
    }
    
    
    // Initiate Gradient
    fileprivate func addGradient() {
        
        gradient.colors = [#colorLiteral(red: 0.231372549, green: 0.4588235294, blue: 0.337254902, alpha: 1).cgColor, #colorLiteral(red: 0.6024902463, green: 0.828561008, blue: 0.2135190368, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x:0.0, y:0.5)
        gradient.endPoint = CGPoint(x:1.0, y:0.5)
        gradient.drawsAsynchronously = true
        gradient.frame = coloredView.bounds
        coloredView.layer.addSublayer(gradient)
        animateGradient()
    }
    
    
    // Animate Color Change
    fileprivate func animateGradient() {
        
        let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
        gradientChangeAnimation.duration = 0
        gradientChangeAnimation.toValue = [#colorLiteral(red: 0.231372549, green: 0.4588235294, blue: 0.337254902, alpha: 1).cgColor, #colorLiteral(red: 0.6024902463, green: 0.828561008, blue: 0.2135190368, alpha: 1).cgColor]
        gradientChangeAnimation.fillMode = kCAFillModeBoth
        gradientChangeAnimation.isRemovedOnCompletion = false
        gradientChangeAnimation.delegate = self
        gradient.add(gradientChangeAnimation, forKey: "colorChange")
    }
}






// MARK: Events
extension LoadingView {
    
    func startAnimate() {
        
        addGradient()
        float()
    }
    
    
    func stopAnimate() {
        
        gradient.removeFromSuperlayer()
        gradient = CAGradientLayer()
    }
}






// Support Continuos Animation
extension LoadingView: CAAnimationDelegate {
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        
        if flag {
            
            gradient.colors = [#colorLiteral(red: 0.231372549, green: 0.4588235294, blue: 0.337254902, alpha: 1).cgColor, #colorLiteral(red: 0.6024902463, green: 0.828561008, blue: 0.2135190368, alpha: 1).cgColor]
            animateGradient()
        }
    }
}
