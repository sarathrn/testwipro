//
//  FeedCollectionViewCell.swift
//  TestWipro
//
//  Created by Sarath R on 26/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

/* Expected Cell Alignemnt
 
 -------------------------------
 |                             |
 |          Image              |
 |                             |
 |Title                        |
 |-----------------------------|
 |Description                  |
 -------------------------------
 
 */

import UIKit




class FeedCollectionViewCell: BaseCollectionViewCell {
    
    
    // Declarations
    static var identifier: String {
    
        return String(describing: self).components(separatedBy: ".").last!
    }
    fileprivate let KDefaultPadding: CGFloat = 8
    fileprivate var feedDescriptionTopAnchor: NSLayoutConstraint!
    fileprivate var feedDescriptionBottomAnchor: NSLayoutConstraint!
    
    
    
    // View Elements
    fileprivate lazy var containerView: UIView = {
        
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.9764705882, green: 0.9764705882, blue: 0.9764705882, alpha: 1)
        return view
    }()
    
    fileprivate lazy var feedImageView: UIImageView = {
        
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    fileprivate let feedTitleLabel: PaddingLabel = {
       
        let label = PaddingLabel()
        label.numberOfLines = 2
        label.textColor = .white
        label.font = UIFont(name: "Avenir Book", size: 22)
        label.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        return label
    }()
    
    fileprivate let feedDescriptionLabel: UILabel = {
        
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont(name: "Avenir Book", size: 17)
        // Content Priority
        label.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: .vertical)
        label.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
        label.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .vertical)
        label.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: .horizontal)
        return label
    }()
    
    
    
    
    // From Parent
    var feed: Feed? {
        
        didSet {
            
            // Set Data & Alignment If Needed
            if let urlString = feed?.imageHref, let url = URL(string: urlString) {
                
                feedImageView.loadImageFrom(url)
            } else {
                
                feedImageView.image = #imageLiteral(resourceName: "image_not_found")
            }
            
            feedTitleLabel.text = feed?.title ?? nil
            if let feedDescription = feed?.description {
            
                feedDescriptionLabel.text =  feedDescription
                addFeedDescriptionMargins()
            } else {
                
                feedDescriptionLabel.text = nil
                removeFeedDescriptionMargins()
            }
        }
    }

    
    
    
    
    // MARK: Life Cycle
    override func configureView() {
        
        // Add view
        addSubview(containerView)
        containerView.addSubview(feedImageView)
        feedImageView.addSubview(feedTitleLabel)
        containerView.addSubview(feedDescriptionLabel)
        
        // Align View
        addConstraintWithFormat("H:|[v0]|", views: containerView)
        addConstraintWithFormat("V:|-(1)-[v0]-(1)-|", views: containerView)

        containerView.addConstraintWithFormat("H:|[v0]|", views: feedTitleLabel)
        containerView.addConstraintWithFormat("H:|[v0]|", views: feedImageView)
        containerView.addConstraintWithFormat("H:|-(8)-[v0]-(8)-|", views: feedDescriptionLabel)
        feedImageView.addConstraintWithFormat("V:[v0(>=40)]|", views: feedTitleLabel)
        containerView.addConstraintWithFormat("V:|[v0]", views: feedImageView)
        
        feedDescriptionTopAnchor = feedDescriptionLabel.topAnchor.constraint(equalTo: feedImageView.bottomAnchor, constant: KDefaultPadding)
        feedDescriptionTopAnchor.isActive = true
        feedDescriptionBottomAnchor = feedDescriptionLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -KDefaultPadding)
        feedDescriptionBottomAnchor.isActive = true
   }
    
    
    fileprivate func removeFeedDescriptionMargins() {
        
        feedDescriptionTopAnchor.constant = 0
        feedDescriptionBottomAnchor.constant = 0
    }
    
    
    fileprivate func addFeedDescriptionMargins() {
        
        feedDescriptionTopAnchor.constant = KDefaultPadding
        feedDescriptionBottomAnchor.constant = -KDefaultPadding
    }
    
}































