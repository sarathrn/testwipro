//
//  EmptyFeedCollectionViewCell.swift
//  TestWipro
//
//  Created by Sarath R on 26/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import UIKit

class EmptyFeedCollectionViewCell: BaseCollectionViewCell {
 
    // Declarations
    static var identifier: String {
        
        return String(describing: self).components(separatedBy: ".").last!
    }
    
    fileprivate let titleLabel: UILabel = {
        
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont(name: "Avenir Book", size: 17)
        label.text = Strings.shared.emptyFeed
        return label
    }()
    
    
    
    
    // MARK: Life Cycle
    override func configureView() {
        
        // Add View
        addSubview(titleLabel)
        
        addConstraintWithFormat("H:|-(16)-[v0]-(16)-|", views: titleLabel)
        addConstraintWithFormat("V:|-(16)-[v0]-(16)-|", views: titleLabel)
    }
}
