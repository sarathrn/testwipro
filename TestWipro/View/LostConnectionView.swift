//
//  LostConnectionView.swift
//  TestWipro
//
//  Created by Sarath R on 27/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation
import UIKit


class LostConnectionView: NSObject {
    
    // Declaration
    static let shared = LostConnectionView()
    
    // UI Elements
    fileprivate let contentView = UIView()
    fileprivate lazy var imageView: UIImageView = {
       
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "no_connection")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    
    
    
    // MARK: Life Cycle
    private override init() {
        super.init()
    }
    
    
    
    func show() {
        
        if let window = UIApplication.shared.keyWindow {
            
            // Add View
            window.addSubview(contentView)
            window.addSubview(imageView)
            contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hide)))
            contentView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
            
            // Estimate Image Height
            let height: CGFloat = (UIScreen.main.bounds.width) * 9 / 16
            let y = window.frame.height - height
            imageView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
            
            // Prepare Animation
            contentView.frame = window.frame
            contentView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.contentView.alpha = 1
                self.imageView.frame = CGRect(x: 0, y: y, width: self.imageView.frame.width, height: self.imageView.frame.height)
            }, completion: nil)
        }
    }

    
    
    @objc func hide() {
      
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            // Reset View
            self.contentView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
            
                self.imageView.frame = CGRect(x: 0, y: window.frame.height, width: self.imageView.frame.width, height: self.imageView.frame.height)
            }
        }, completion: nil)
    }
}
