//
//  ViewController.swift
//  TestWipro
//
//  Created by Sarath R on 26/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import UIKit

class FeedsViewController: UICollectionViewController {
    
    
    // Declarations
    fileprivate let kDefaultPageTitle = "Welcome"
    fileprivate let KDefaultPadding: CGFloat = 8
    var dataSource: FeedsModal?
    
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        configureCollectionView()
        loadData()
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        refreshLayout()
    }
    
    
    // MARK: Arrange View
    func configureView() {
        
        navigationController?.navigationBar.isTranslucent = false
        updatePageTitle()
    }
    
    
    func configureCollectionView() {
        
        if let layout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            
            layout.scrollDirection = .vertical
            
            /*  Safe Area Alignment Adjust for iPhone X */
            if #available(iOS 11.0, *) {
             
                layout.sectionInsetReference = .fromSafeArea
            }
        }
        collectionView?.backgroundColor = UIColor.white.withAlphaComponent(0.9)
        
        // Register Cells
        collectionView?.register(FeedCollectionViewCell.self, forCellWithReuseIdentifier: FeedCollectionViewCell.identifier)
        collectionView?.register(EmptyFeedCollectionViewCell.self, forCellWithReuseIdentifier: EmptyFeedCollectionViewCell.identifier)
        addRefresController()
    }
    
    
    func addRefresController() {
        
        if #available(iOS 10.0, *) {
            
            let refreshController = UIRefreshControl()
            refreshController.addTarget(self, action: #selector(FeedsViewController.loadData), for: UIControlEvents.valueChanged)
            refreshController.tintColor = .clear
            collectionView?.refreshControl = refreshController
        } else {
            
            let leftbarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "refresh"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(FeedsViewController.loadData))
            navigationItem.rightBarButtonItem = leftbarButton
            navigationItem.rightBarButtonItem?.tintColor = .black
        }
    }
}






// Support Server Request
extension FeedsViewController {
    
    
    @objc func loadData() {
        
        showLoadingIndicator()
        FeedDataController().load { (response, status) in
            
            // Validation
            switch status {
                
            case .noConnection:
                LostConnectionView.shared.show()
                
            case .emptyResponse, .error:
                break
                
            case .success:
                if let data = response {
                    
                    self.dataSource = data
                }
            }
            self.refresh()
        }
    }
    
    
    
    func refresh() {
        
        endLoading()
        updatePageTitle()
        collectionView?.reloadData()
    }
    
    
    
    func refreshLayout() {
        
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    
    func endLoading() {
        
        if #available(iOS 10.0, *) {
        
            collectionView?.refreshControl?.endRefreshing()
        }
        hideLoadingIndicator()
    }
    
    
    func updatePageTitle() {
        
        self.title = dataSource?.pageTitle ?? kDefaultPageTitle
    }
    
}








// Support Listing
extension FeedsViewController {
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if let feeds = dataSource?.feeds, feeds.count > 0 {
            
            return feeds.count
        } else {
            
            return 1
        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Validation
        guard let feeds = dataSource?.feeds, feeds.count > 0 else {
            
            let emptyCell = collectionView.dequeueReusableCell(withReuseIdentifier: EmptyFeedCollectionViewCell.identifier, for: indexPath) as! EmptyFeedCollectionViewCell
            return emptyCell
        }
        
        // Preapre Data Cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeedCollectionViewCell.identifier, for: indexPath) as! FeedCollectionViewCell
        cell.feed = feeds[indexPath.row]
        return cell
    }

}




// Preapre Display- Cell Sizing
extension FeedsViewController:  UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return KDefaultPadding * 0.5
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(KDefaultPadding, KDefaultPadding, KDefaultPadding, KDefaultPadding)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // No Feeds
        guard let feeds = dataSource?.feeds, feeds.count > 0 else {
            
           return cellSizeForEmptyCell()
        }
        
        // Has Feeds
        return cellSizeForDataCell(feeds[indexPath.item])
    }
    
}





// Helper Cell Sizing
extension FeedsViewController {
    
    
    /*  View Alignemnt
     *
     *  |-(8)--(16)---label---(16)--(8)-|
     *
     *  Collection view padding = 8
     *  label padding = 16
     */
    
    func cellSizeForEmptyCell() -> CGSize {
        
        let cellWidth = UIScreen.main.bounds.width
        var cellHeight = estimatedDecriptionLabelFrame(cellWidth: cellWidth, Strings.shared.emptyFeed, margin: KDefaultPadding * 3).height
        cellHeight += KDefaultPadding + (KDefaultPadding * 2) + (KDefaultPadding * 2) + KDefaultPadding
        return CGSize(width: cellWidth - KDefaultPadding - KDefaultPadding, height: cellHeight)
    }
    
    
    
    func cellSizeForDataCell(_ feed: Feed) -> CGSize {
        
        // Estimate Width
        let screenWidth = UIScreen.main.bounds.width
        var cellWidth = screenWidth
        if screenWidth > 414 {
            
            cellWidth = screenWidth * 0.5
        }
        
        // Adjust Safe area margin for iPhone X
        if #available(iOS 11.0, *) {
            
            let left = view.safeAreaInsets.left
            cellWidth = cellWidth - left
        }
        // Adjust cell margins
        cellWidth = cellWidth - KDefaultPadding - KDefaultPadding
        
        
        
        
        // Estimate Height
        let imageHeight = (cellWidth) * 3 / 4
        var cellHeight = imageHeight
        
        if let feedDescription = feed.description {
            
            let actualFrame = estimatedDecriptionLabelFrame(cellWidth: cellWidth, feedDescription, margin: KDefaultPadding)
            cellHeight += actualFrame.height
        }
        
        // (Add all margin spaces)
        cellHeight +=  1 + KDefaultPadding + KDefaultPadding + 1
        
        
        
        // Updated cell Height
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
    
    
    // Calculate Label Frame respect with content size
    func estimatedDecriptionLabelFrame(cellWidth: CGFloat, _ text: String, margin: CGFloat) -> CGRect {
        
        // Label width = cellwidth - leftmargin - rightmargin
        let approximateSize = CGSize(width: (cellWidth - margin - margin), height: 1000)
        let attributes = [NSAttributedStringKey.font: UIFont(name: "Avenir Book", size: 17)!]
        
        let actualDescriptionFrame = NSString(string: text).boundingRect(with: approximateSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes, context: nil)
        return actualDescriptionFrame
    }
    
}









