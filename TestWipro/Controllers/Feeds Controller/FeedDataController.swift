//
//  FeedDataController.swift
//  TestWipro
//
//  Created by Sarath R on 27/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation


class FeedDataController {
    
    
    func load(_ completionHandler: @escaping (_ feed: FeedsModal?, _ error: NetworkResponseStatus) -> ()) {
        
        NetworkRequestManager.shared.get(NetworkConnection.shared.feeds, params: [:]) { (status, response) in

            // Validate Status
            switch status {
                
            case .noConnection:
                completionHandler(nil, .noConnection)
                
            case .success:
                 self.validateResponse(response, completionHandler: completionHandler)
                
            case .error:
                 completionHandler(nil, .error)
                
            case .emptyResponse:
                 completionHandler(nil, .emptyResponse)
            }
            
        }
    }
    
    
    fileprivate func validateResponse(_ data: Data?, completionHandler: (_ feed: FeedsModal?, _ error: NetworkResponseStatus) -> ()) {
        
        if let _data = data, let responseData = preapreUTF8SupportableData(_data) {
            
            if let result = parse(responseData) {
                
                completionHandler(result, .success)
                
            } else {
                
                completionHandler(nil, .emptyResponse)
            }
            
        } else {
            
            completionHandler(nil, .emptyResponse)
        }
    }
    
    
    /*   Rectify Data Error
     *   Currently the result json seems broken, not supporting utf8 conversion;
     *   System throwing - Unable to convert data to string around character 2643
     *
     *   We can remove the following function as soon as server data came in correct format
     */
    
    fileprivate func preapreUTF8SupportableData(_ data: Data)-> Data? {
        
        let response = String(data: data, encoding: String.Encoding.ascii)
        guard let newData = response?.data(using: String.Encoding.utf8) else {
            
            if logActivity {
                
                print("could not convert data to UTF-8 format; Server must provide valid UTF8 supportable JSON")
            }
            return nil
        }
        return newData
    }
    
}



// Support Parsing
extension FeedDataController {
    
    // Parse Feeds & Prepare Modal
    func parse(_ data: Data) -> FeedsModal? {
        
        do {
            
            let result = try JSONDecoder().decode(FeedsModal.self, from: data)
            return result
        } catch {
            
            if logActivity {
                
                print("FeedsModal", error)
            }
        }
        return nil
    }

}

