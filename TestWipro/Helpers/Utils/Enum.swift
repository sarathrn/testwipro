//
//  Enum.swift
//  TestWipro
//
//  Created by Sarath R on 27/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation


enum NetworkResponseStatus {
    
    case success, error, noConnection, emptyResponse
}
