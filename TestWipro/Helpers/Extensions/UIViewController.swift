//
//  UIViewController.swift
//  TestWipro
//
//  Created by Sarath R on 26/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    
    func showLoadingIndicator() {
        
        hideLoadingIndicatorWithNoTime()
        prepareNewLoadingView()
    }
    
    
    func hideLoadingIndicator() {
        
        let loadingView = view.subviews.filter { $0 is LoadingView }
        UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            
            loadingView.first?.alpha = 0
        }) { (status) in
            
            loadingView.first?.removeFromSuperview()
        }
    }
    
    
    func updateLoadingIndicatorFrame() {
        
        let loadingView = view.subviews.filter { $0 is LoadingView }
        if !loadingView.isEmpty {
            
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                
                self.hideLoadingIndicatorWithNoTime()
            }) { (status) in
                
                self.showLoadingIndicator()
            }
        }
    }
    
    fileprivate func prepareNewLoadingView() {
        
        let barIndicatorView = LoadingView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 4))
        view.addSubview(barIndicatorView)
        view.bringSubview(toFront: barIndicatorView)
    }
    
    // Immedidate removal
    fileprivate func hideLoadingIndicatorWithNoTime() {
        
        let loadingView = view.subviews.filter { $0 is LoadingView }
        loadingView.first?.removeFromSuperview()
    }
}
