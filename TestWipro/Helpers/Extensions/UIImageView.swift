//
//  UIImageView.swift
//  TestWipro
//
//  Created by Sarath R on 26/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage


extension UIImageView {
    
    
    /*  Load image Asynchornusly from remote URL */
    func loadImageFrom(_ url: URL) {
        
        setShowActivityIndicator(true)
        setIndicatorStyle(UIActivityIndicatorViewStyle.gray)
        sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "image_not_found"), options: SDWebImageOptions.refreshCached) { (image, error, cacheType, url) in
            
            if let _ = error {
                
                self.image = #imageLiteral(resourceName: "image_not_found")
            }
        }
    }
    
    
    func blur() {
        
        let effect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
        let effectView = UIVisualEffectView(effect: effect)
        effectView.frame = bounds
        effectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(effectView)
    }
}
