//
//  NetworkRequestManager.swift
//  TestWipro
//
//  Created by Sarath R on 26/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation
import Alamofire


class NetworkRequestManager: NSObject {
    
    // Declarations
    static let shared = NetworkRequestManager()
    let urls = NetworkConnection.shared
    
    
    // MARK: Life Cycle
    private override init() {
        super.init()
    }
    
    
    // GET Request
    func get(_ url: String, params: [String: Any], completionHandler: @escaping (_ status: NetworkResponseStatus, _ response: Data?) -> ()) {
        
        // Validation
        guard isConnected() else {
            
            completionHandler(.noConnection, nil)
            return
        }
        
        // Send Request
        Alamofire.request(makeConnectionUrl(url), method: HTTPMethod.get, parameters: params, encoding: JSONEncoding.default).validate().responseData(completionHandler: { (response) in
            
            // Log
            if logActivity {
                
                if let data = response.data {
                    
                    print(String(data: data, encoding: String.Encoding.ascii) ?? "NO Data")
                }
            }
            
            
            // Validate result
            switch response.result {
                
            case .success:
                
                // Notify Parent
                completionHandler(.success, response.data)
                
            case .failure(let error):
                
                if logActivity {
                    
                    print(error)
                }
                
                // Notify Parent
                completionHandler(.error, response.data)
            }
            
        })
        
    }
    
}





// Helper Methods
extension NetworkRequestManager {
    
    
    // Read Internet Connection Status
    fileprivate func isConnected() -> Bool {
        
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    
    fileprivate func makeConnectionUrl(_ url: String) -> String {
        
        return urls.domain + url
    }
}
