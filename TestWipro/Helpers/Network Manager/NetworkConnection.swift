//
//  NetworkConnection.swift
//  TestWipro
//
//  Created by Sarath R on 26/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation


class NetworkConnection: NSObject {
    
    // Declarations
    static let shared = NetworkConnection()
    
    
    // MARK: Life Cycle
    private override init() {
        super.init()
    }
    
    
    let domain = "https://dl.dropboxusercontent.com/"
    let feeds = "s/2iodh4vg0eortkl/facts.json"
}
