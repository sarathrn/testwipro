//
//  FeedsModal.swift
//  TestWipro
//
//  Created by Sarath R on 26/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation



struct FeedsModal: Decodable {
    
    var pageTitle: String?
    var feeds: [Feed]?
    
    enum CodingKeys: String, CodingKey {
        
        case pageTitle = "title"
        case feeds = "rows"
    }
}


struct Feed: Decodable {
    
    var title: String?
    var description: String?
    var imageHref: String?
}





