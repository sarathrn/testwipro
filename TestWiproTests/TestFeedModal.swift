//
//  TestFeed.swift
//  TestWiproTests
//
//  Created by Sarath R on 27/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation
import XCTest
@testable import TestWipro

class TestFeed: XCTestCase {
    
    // MARK: Life Cycle
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        super.tearDown()
        
    }
    
    
    // MARK: Feed Mapping Tester
    func testFeedMapping() throws {
        
        // Load Json
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "feed", withExtension: "json") else {
        
            XCTFail("Feeds file not found!")
            return
        }
        
        // Map Json
        let jsonData = try Data(contentsOf: url)
        let feedModal = try JSONDecoder().decode(FeedsModal.self, from: jsonData)
        
        // Perform Test
        XCTAssertEqual(feedModal.pageTitle, "About Canada")
        XCTAssertEqual(feedModal.feeds?.count, 14)
        XCTAssertEqual(feedModal.feeds?.first?.title, "Beavers")
    }
   
}
