//
//  TestNetwork.swift
//  TestWiproTests
//
//  Created by Sarath R on 27/07/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import TestWipro
class TestNetworkFeed: QuickSpec {
    
    // MARK: Life Cycle
    override func spec() {
        super.spec()
        
        feedLoading()
    }
    
    
    // MARK: Test Feed Loading From Server
    func feedLoading() {
        
        describe("NetworkRequest: Load feeds from server") {
            
            // ------------------------------------------- //
            context("No Internet Connection") {
                
                it("Return empty FeedsModal") {
                    
                    var feedData: FeedsModal?
                    FeedDataController().load({ (feedsModal, status) in
                        
                        if status == NetworkResponseStatus.noConnection {
                            
                            feedData = feedsModal
                        }
                    })
                    expect(feedData).toEventually(beNil(), timeout: 20)
                }
            }
            
            
            // ------------------------------------------- //
            context("Response Success") {
                
                it("Valid JSON & Parsing success: Should returns valid FeedsModal") {
                    
                    var feedData: FeedsModal?
                    FeedDataController().load({ (feedsModal, status) in
                        
                        if status == .success {
                            
                            feedData = feedsModal
                            
                            // Validate feeds
                            expect(feedData?.pageTitle) == "About Canada"
                            expect(feedData?.feeds).to(haveCount(14))
                            expect(feedData?.feeds?.first?.title) == "Beavers"
                            expect(feedData?.feeds?.first?.description).to(contain("A group of beavers is called a colony"))
                            expect(feedData?.feeds?.first?.imageHref).to(contain("American_Beaver.jpg"))
                            
                        }
                    })
                    
                    // Validate response as expected
                    expect(feedData).toEventuallyNot(beNil(), timeout: 20)
                }
                
                
                it("Invalid JSON & Pasring fails: Should return empty FeedsModal") {
                    
                    var feedData: FeedsModal?
                    FeedDataController().load({ (feedsModal, status) in
                        
                        if status == NetworkResponseStatus.emptyResponse {
                            
                            feedData = feedsModal
                        }
                    })
                    
                    expect(feedData).toEventually(beNil(), timeout: 20)
                }
            }
        }
        
        
        // ------------------------------------------- //
        context("Response Error") {
            
            it("When response is Error: Should return empty FeedsModal") {
                
                var feedData: FeedsModal?
                FeedDataController().load({ (feedsModal, status) in
                    
                    if status == NetworkResponseStatus.error {
                        
                        feedData = feedsModal
                    }
                })
                expect(feedData).toEventually(beNil(), timeout: 20)
            }
        }
    }

    
}



